const { executeQuery } = require("../database/database")

async function actorService(){
    let movieapp=await executeQuery('SELECT actorName,age,movieName FROM actor inner join movie on actor.actor_id= movie.actor_id')

    return movieapp[0]
}
async function actorCheckIdService(id){
   let checkId=await executeQuery(`SELECT actor_id FROM actor where actor_id=${id}`)
   console.log("CheckActorId",checkId[0]);
   if(checkId[0].length>0){
    let movieapp=await executeQuery(`SELECT actorName,actor.age,movieName FROM actor inner join movie on actor.actor_id= movie.actor_id where actor.actor_id=${id}`)
    return movieapp[0]
   }else{
       return "ID NOT FOUND"
   }
    
}
async function directorService(){
    let movieapp=await executeQuery('SELECT directorName,age,movieName FROM director inner join movie on director.director_id= movie.director_id')
    return movieapp[0]
}
async function directorCheckIdService(id){
    let movieapp=await executeQuery(`SELECT directorName,movieName FROM director inner join movie on director.director_id= movie.director_id where director.director_id=${id}`)
    return movieapp[0];
}
async function movieService(){
    let movieapp=await executeQuery('SELECT movieName ,actorName,directorName,releasedYear from movie inner join actor on actor.actor_id= movie.actor_id inner join director on director.director_id= movie.director_id ')
    return movieapp[0];

}
async function movieCheckIdService(id){
    let movieapp=await executeQuery(`SELECT movieName,directorName,actorName FROM movie inner join actor on actor.actor_id= movie.actor_id inner join
    director on  director.director_id=movie.director_id where movie.movie_id=${id}`)
    return movieapp[0];
}
async function actorCreateService(actorName,age){
            console.log("NAME/AGE",actorName,age);
            let checkName=await executeQuery(`Select actorName from actor where actor.actorName="${actorName}" and actor.age=${age}`)
            console.log("Actor NAME",checkName);  
            if(checkName[0].length==0){  
            let movieapp=await executeQuery(`INSERT INTO actor(actorName, age) VALUES ("${actorName}",${age})`);
        console.log("CREATe actor",movieapp);
        return "CREATED";
            }else{
                return "ACTOR ALREADY EXIST"
            }
}
async function actorDeleteService(id){
    console.log("Id to be deleted",id);
    let checkId=await executeQuery(`SELECT actor_id FROM MovieApp.actor where actor.actor_id=${id}`)
    console.log("DAtabase checkId",checkId[0]);
    if(checkId[0].length>0){
    let movieapp=await executeQuery(`DELETE from actor where actor_id=${id}`)
    console.log("Deleted ID",id);
    return "DELETED"
    }else{
        return "ID TO BE DELETED NOT FOUND"
    }
}
async function actorUpdateService(id,actorName,age){
    let checkId=await executeQuery(`SELECT actor_id FROM MovieApp.actor where actor.actor_id=${id}`)
    console.log("DAtabase checkId",checkId[0]);
    if(checkId[0].length>0){
        let movieapp=await executeQuery(`Update actor set actorName="${actorName}",age=${age} where actor_id=${id}`)
        console.log("Update ID",id);
        return "UPDATED"
        }else{
            return "ID TO BE UPDATED NOT FOUND"
        }
}
async function directorCreateService(directorName,age){
    console.log("NAME/AGE",directorName,age);
    let checkName=await executeQuery(`Select directorName from director where director.directorName="${directorName}"`)
    console.log("DirNAME",checkName);
    if(checkName[0].length==0){
        let movieapp=await executeQuery(`INSERT INTO director(directorName, age) VALUES ("${directorName}",${age})`);
        console.log("CREATe director",movieapp);
        return "DIRECTOR CREATED";
    }else{
        return "DIRECTOR ALREADY EXIST"
    }
}
async function directorDeleteService(id){
    console.log("Id to be deleted",id);
    let checkId=await executeQuery(`SELECT director_id FROM MovieApp.director where director.director_id=${id}`)
    console.log("DAtabase checkId",checkId[0]);
    if(checkId[0].length>0){
        let movieapp=await executeQuery(`DELETE from director where director_id=${id}`)
        console.log("Deleted ID",id);
        return "Director DELETED"
        }else{
            return "ID TO BE DELETED NOT FOUND"
        }

}
async function directorUpdateService(id,directorName,age){
    let checkId=await executeQuery(`SELECT director_id FROM MovieApp.director where director.director_id=${id}`)
    console.log("DAtabase checkId",checkId[0]);
    if(checkId[0].length>0){
        let movieapp=await executeQuery(`Update director set directorName="${directorName}",age=${age} where director_id=${id}`)
        console.log("Update ID",id);
        return "UPDATED"
        }else{
            return "ID TO BE UPDATED NOT FOUND"
        }
}
async function movieDeleteService(id){
    console.log("Id to be deleted",id);
    let checkId=await executeQuery(`SELECT movie_id FROM MovieApp.movie where movie.movie_id=${id}`)
    console.log("DAtabase checkId",checkId[0]);
if(checkId[0].length>0){
    let movieapp=await executeQuery(`DELETE from movie where movie_id=${id}`)
    console.log("Deleted ID",id);
    return "Movie DELETED"
    }else{
        return "ID TO BE DELETED NOT FOUND"
    }
}
async function movieCreateSevice(movieName,releasedYear,director_id,actor_id){
    console.log("MOVIE CREATE",movieName,releasedYear,director_id,actor_id)
//     for(let i=0;i<actor_id.length;i++){
let movieapp=await executeQuery(`INSERT INTO MovieApp.movie(director_id,movieName,releasedYear) VALUES (${director_id},"${movieName}",${releasedYear})`)
    
console.log("MoviE CREAte",movieapp);
//     }
    return "CREATED"

}
async function movieActorCreateService(movie_id,actor_id){
//     console.log("MovieActorcreate",movie_id,actor_id.length );
//     for(let i=0 ;i < actor_id.length; i++){
//     let addActorMovie=await executeQuery(`INSERT INTO MovieApp.movieactor(movie_id,actor_id) VALUES (${movie_id},${actor_id[i]})`)
//     // let movieapp= await executeQuery(`INSERT INTO MovieApp.movie(actor_id) VALUES(${actor_id[i]}) where movie.movie_id=${movie_id}`);
// //    let movieapp=await executeQuery(`UPDATE movie SET actor_id= ${actor_id[i]} where movie.movie_id=${movie_id} `)
//     console.log("addActorMovie",addActorMovie);
//     }
//     let updateactor=await executeQuery('UPDATE movie JOIN movieactor on movie.movie_id=movieactor.movie_id SET movie.actor_id=movieactor.actor_id')
//     return "Actors added"
}

module.exports={actorService,actorCheckIdService,directorService,directorCheckIdService,
    movieService,movieCheckIdService,actorCreateService,actorDeleteService,
    actorUpdateService,directorCreateService,directorDeleteService,directorUpdateService,
    movieDeleteService,movieCreateSevice,movieActorCreateService}