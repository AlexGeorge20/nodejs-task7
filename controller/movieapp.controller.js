const { actorService, directorService, movieService, actorCreateService, actorCheckIdService, directorCheckIdService, movieCheckIdService, actorDeleteService, actorUpdateService, directorCreateService, directorDeleteService, directorUpdateService, movieDeleteService, movieCreateSevice, movieActorCreateService } = require("../service/movieapp.service");



const actorController= async ( req,res )=>{
    const response=await actorService();
    console.log("Actors",response);
    res.send(response);
}
const actorCheckIdController=async(req,res)=>{
    const id=req.params.id;
    const response=await actorCheckIdService(id);
    console.log("Actors by Id",response);
    res.send(response)
}

const directorController=async (req,res)=>{
    const response=await  directorService();
    console.log("Directors",response);
    res.send(response);
}
const directorCheckIdController=async(req,res)=>{
    const id=req.params.id;
    const response=await directorCheckIdService(id);
    console.log("Directors by Id",response);
    res.send(response)
}
const movieController=async(req,res)=>{
    const response= await movieService();
    console.log("Movies",response);
    res.send(response)
}
const movieCheckIdController=async(req,res)=>{
    let id=req.params.id;
    const response=await movieCheckIdService(id);
    console.log("Movies by Id",response);
    res.send(response)
}
const actorCreateController=async(req,res)=>{
    
        const actorName= req.body.actorName;
        const age=req.body.age;
    const response=await actorCreateService(actorName,age);
    console.log("CReated actor",response);
    res.send(response)
}
const actorDeleteController=async(req,res)=>{
    const id= req.params.id;
    const response=await actorDeleteService(id);
    console.log("Deleted actor",response);
    res.send(response)
}
const actorUpdateController=async(req,res)=>{
    const id=req.params.id;
    const actorName=req.body.actorName;
    const age=req.body.age;
    const response=await actorUpdateService(id,actorName,age);
    console.log("Update actor",response);
    res.send(response)
}
const directorCreateController=async(req,res)=>{
    const directorName= req.body.directorName;
    const age=req.body.age;
const response=await directorCreateService(directorName,age);
console.log("CReated director",response);
res.send(response)
}
const directorDeleteController=async(req,res)=>{
    const id= req.params.id;
    const response=await directorDeleteService(id);
    console.log("Deleted director",response);
    res.send(response)
}

const directorUpdateController=async(req,res)=>{
    const id=req.params.id;
    const directorName=req.body.directorName;
    const age=req.body.age;
    const response=await directorUpdateService(id,directorName,age);
    console.log("Update director",response);
    res.send(response)
}
const movieDeleteController=async(req,res)=>{
    const id= req.params.id;
    const response=await movieDeleteService(id);
    console.log("Deleted movie",response);
    res.send(response)
}
const movieCreateController=async(req,res)=>{
    const movieName=req.body.movieName;
    const releasedYear=req.body.releasedYear;
    const director_id=req.body.director_id;
    const actor_id=req.body.actor_id;
    const response=await movieCreateSevice(movieName,releasedYear,director_id,actor_id)
    res.send(response)
}
const movieActorCreateController=async(req,res)=>{
        const movie_id=req.params.id;
        const actor_id=req.body.actor_id;
        const response=await movieActorCreateService(movie_id,actor_id)
        res.send(response)
}


module.exports={actorController,actorCheckIdController,directorController,
    directorCheckIdController,movieController,movieCheckIdController,
    actorCreateController,actorDeleteController,actorUpdateController,directorCreateController,
    directorDeleteController,directorUpdateController,movieDeleteController,movieCreateController,
    movieActorCreateController}