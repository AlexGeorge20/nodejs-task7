
const express= require('express');
const { movieappRouter } = require('./route/movieapp.route');
const app=express();


app.use(express.json())
app.use(movieappRouter)


app.listen(8080,()=>{
    console.log('App is running');
})
