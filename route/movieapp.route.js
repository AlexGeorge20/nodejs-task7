const{Router}=require('express');
const { actorController, directorController, movieController, actorCreateController, actorCheckIdController, directorCheckIdController, movieCheckIdController, actorDeleteController, actorUpdateController, directorCreateController, directorDeleteController, directorUpdateController, movieDeleteController, movieCreateController, movieActorCreateController } = require('../controller/movieapp.controller');

const router= Router();

let path='/movieapp'
//Read Actors
router.get(`${path}/actors`,actorController)
//Read actor by Id
router.get(`${path}/actors/:id`,actorCheckIdController)
//Read Directors
router.get(`${path}/directors`,directorController)
//Read director by Id
router.get(`${path}/directors/:id`,directorCheckIdController)
//Read Movies
router.get(`${path}/movies`,movieController)
//Read movies by Id
router.get(`${path}/movies/:id`,movieCheckIdController)

//Create actor
router.post(`${path}/actors`,actorCreateController)
//Delete Actor
router.delete(`${path}/actors/:id`,actorDeleteController)
//Update Actor
router.put(`${path}/actors/:id`,actorUpdateController)

//Create Director
router.post(`${path}/directors`,directorCreateController)
//Delete Director
router.delete(`${path}/directors/:id`,directorDeleteController)
//Update Director
router.put(`${path}/directors/:id`,directorUpdateController)

//Delete Movie
router.delete(`${path}/movies/:id`,movieDeleteController)
//Create Movie
router.post(`${path}/movies`,movieCreateController)
//Create actor for movie
router.post(`${path}/movies/:id`,movieActorCreateController)



module.exports= { movieappRouter:router }